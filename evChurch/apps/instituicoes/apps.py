from django.apps import AppConfig
from django.urls import reverse_lazy
from ..core.menu import Menu


class InstituicoesConfig(AppConfig):
    name = 'evChurch.apps.instituicoes'

    menu = {
        'verbose_name': 'Instituições',
        'permissions': ['instituicoes.add_instituicao'],
        'login_required': 'True',
        'icon': 'church',
        'items': [
            {
                'verbose_name': 'Pesquisar Instituição',
                'link': reverse_lazy('instituicoes:instituicao_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Instituição',
                'link': reverse_lazy('instituicoes:instituicao_create'),
                'permissions': ['add_instituicao'],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)