from django.db import models

class Instituicao(models.Model):
    cnpj = models.CharField(max_length=15, null=True, blank=True, unique=True)
    razao_social = models.CharField(max_length=100, null=True, blank=True)
    natureza_juridica = models.CharField(max_length=200, null=True, blank=True)
    data_fundacao = models.DateField(null=True, blank=True)
    responsavel = models.CharField(max_length=90, null=True, blank=True)
    logradouro = models.CharField(max_length=120, null=True, blank=True)
    numero = models.CharField(max_length=10, null=True, blank=True)
    bairro = models.CharField(max_length=100, null=True, blank=True)
    complemento = models.CharField(max_length=80, blank=True, null=True)
    cep = models.CharField(max_length=9, null=True, blank=True)
    cidade = models.CharField(max_length=100, null=True, blank=True)
    telefone = models.CharField(max_length=12, null=True, blank=True)
    uf = models.CharField(max_length=2, null=True, blank=True)

    def __str__(self):
        return self.razao_social
