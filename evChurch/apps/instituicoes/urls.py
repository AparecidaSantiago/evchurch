from django.urls import path, include
from . import views

app_name='instituicoes'

urlpatterns = [
    path("instituicao", views.InstituicaoView.as_view(), name="instituicao_index"),
    path("instituicao/detalhes/<int:pk>/",
        views.InstituicaoDetailView.as_view(), name="instituicao_show"),
    path("instituicao/cadastrar/",views.instituicao_create, name="instituicao_create"),
    path("instituicao/<int:pk>/editar/",views.instituicao_edit, name="instituicao_edit"),
    path("instituicao/<int:pk>/excluir/", views.instituicao_delete, name="instituicao_delete")
]
