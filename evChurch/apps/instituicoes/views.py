from django.shortcuts import render
from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from .forms import *
from .models import Instituicao
from django.http import HttpResponse

@method_decorator(login_required, name='dispatch')
class InstituicaoView(SearchCustomView):
    """
    View para consulta de instituições
    """
    form_class = SearchInstituicao
    model = Instituicao
    order_field = "razao_social"
    template_name = "instituicoes/index.html"
    page_title = "Instituições"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("instituicoes:instituicao_index"), "Instituições")
    ]

    def get_queryset(self):
        """
        Altera o filtro da instituição ::
        """
        queryset = super(InstituicaoView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class InstituicaoDetailView(CustomDetailView):
    """
    View para exibição dos detalhes da instituição
    """
    model = Instituicao
    template_name = "instituicoes/detail.html"
    page_title = "Isntituições"

    def get_context_data(self, **kwargs):
        context = super(InstituicaoDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("instituicoes:instituicao_index"), "Instituições"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
@permission_required("instituicoes.add_instituicao", raise_exception=True)
def instituicao_create(request):
    """View para Criar uma instituição"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("instituicoes:instituicao_index"), "Instituição"),
        (reverse("instituicoes:instituicao_create"), "Nova Instituição")
    ]

    form = InstituicaoCreateForm()
    if request.method == 'POST':
        form = InstituicaoCreateForm(request.POST)

        if form.is_valid():
            instituicao = form.save(commit=False)
            instituicao.save()

            messages.success(request, "Instituição cadastrada com sucesso")
            return redirect(reverse("instituicoes:instituicao_show", args=[instituicao.pk]))

    return render(request, "instituicoes/cadastro.html", locals())

@login_required
@permission_required("instituicoes.add_instituicao", raise_exception=True)
def instituicao_edit(request, pk):
    """"View para Editar as Instituições"""
    instituicao = get_object_or_404(Instituicao, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("instituicoes:instituicao_index"), "Instituções"),
        (reverse("instituicoes:instituicao_show", args=[pk]), instituicao.id),
        ("#", "Editar")
    ]

    form = InstituicaoCreateForm(instance=instituicao)

    if request.method == 'POST':
        form = InstituicaoCreateForm(request.POST, request.FILES, instance=instituicao)

        if form.is_valid():
            instituicao = form.save(commit=False)
            instituicao.save()

            messages.success(request, "Instituição alterada com sucesso.")
            return redirect(reverse("instituicoes:instituicao_show", args=[instituicao.pk]))

    return render(request, "instituicoes/editar.html", locals())

@login_required
@permission_required("instituicoes.add_instituicao", raise_exception=True)
def instituicao_delete(request, pk):
    """
    View responsável por deletar uma instituição
    """
    instituicao = get_object_or_404(Instituicao, pk=pk)

    instituicao.delete()
    messages.success(request, "Instituição deletada com sucesso!")
    return HttpResponse("Instituição deletada com sucesso!")