/*Inicia auto preenchimento de campos de PJ */
$('#id_cnpj').change(function() {
    $.ajax({
    type: 'GET',
    url: 'https://www.receitaws.com.br/v1/cnpj/' + $('#id_cnpj').val(),
    crossDomain: true,
    dataType: 'jsonp',
    data: {"cnpj" : $('#id_cnpj').val()},
    success: function(resposta){
        if (resposta.situacao.includes('ATIVA')){
            console.log($("#id_razao_social").val());
            $("#id_razao_social").val(resposta.nome);
            $("#id_natureza_juridica").val(resposta.natureza_juridica);
            $("#id_data_fundacao").val(resposta.abertura);
        }
        else if (resposta.status.includes('ERROR')){
            $('#alert').fadeIn(1000);
            setTimeout(function() {
               $('#alert').fadeOut(1000);
            }, 5000);
        }
    }
    });
});

//Inicia preenchimento de campos de endereço
$("#id_logradouro").attr('readonly', true);
$("#id_bairro").attr('readonly', true);
$("#id_cidade").attr('readonly', true);
$("#id_uf").attr('readonly', true);

function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('id_logradouro').value=("");
        document.getElementById('id_cidade').value=("");
        document.getElementById('id_bairro').value=("");
        document.getElementById('id_uf').value=("");
}

function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        document.getElementById('id_logradouro').value=(conteudo.logradouro);
        document.getElementById('id_cidade').value=(conteudo.localidade);
        document.getElementById('id_bairro').value=(conteudo.bairro);
        document.getElementById('id_uf').value=(conteudo.uf);
        $("#id_bairro option").filter(function() {
          return this.text == conteudo.bairro;
        }).attr('selected', true);
    } //end if.
    else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
}

function pesquisacep(valor) {

    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            document.getElementById('id_logradouro').value="...";
            document.getElementById('id_cidade').value="...";
            document.getElementById('id_bairro').value=("");
            document.getElementById('id_uf').value=("");

            //Cria um elemento javascript.
            var script = document.createElement('script');

            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);

        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep();
            alert("Formato de CEP inválido. Insira os dados do Endereço manualmente");
            $("#id_logradouro").attr('readonly', false);
            $("#id_bairro").attr('readonly', false);
            $("#id_cidade").attr('readonly', false);
            $("#id_uf").attr('readonly', false);
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};

$("#id_cep").blur(function() {
  cep = $(this).val();
  pesquisacep(cep)
});