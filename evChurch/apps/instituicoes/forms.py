from django import forms
from .models import Instituicao
from localflavor.br.forms import BRCNPJField, BRStateChoiceField, BRCPFField

class SearchInstituicao(forms.Form):
    """Formulário para consulta de instituição da view que lista instituições
    de forma genérica"""

    cnpj = BRCNPJField(label="CNPJ", required=False)
    razao_social = forms.CharField(label="Razão Social", required=False)

class InstituicaoCreateForm(forms.ModelForm):
    """Formulário para criação de instituições"""
    cnpj = BRCNPJField(required=True, label="CNPJ")
    data_fundacao =  forms.CharField(widget=forms.TextInput(attrs={'type':'date'}), required=False, label="Data de Fundação")
    cep = forms.CharField(required=True)
    logradouro = forms.CharField(required=True)
    numero = forms.CharField(required=True, label="Número")
    cidade = forms.CharField(required=True)
    telefone = forms.CharField(required=True)
    bairro = forms.CharField(required=True)
    uf = BRStateChoiceField(required=True, label="UF")

    class Meta:
        model = Instituicao
        fields = ('__all__')
        labels = {
            'razao_social' : 'Razão Social'
        }
