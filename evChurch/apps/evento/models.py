from django.db import models
from ..instituicoes.models import Instituicao

class Evento(models.Model):
    """Modelo responsável por reunir informações sobre a agenda da instituição"""
    instituicao = models.ForeignKey(Instituicao, null=True, blank=True, on_delete=models.CASCADE)
    data_inicial = models.DateField()
    data_final = models.DateField()
    horario_entrada = models.CharField(max_length=5, null=True, blank=True)
    horario_saida = models.CharField(max_length=5, null=True, blank=True)
    responsavel = models.CharField(max_length=100, null=True, blank=True)
    aberto_publico = models.CharField(max_length=3, null=True, blank=True)
    telefone = models.CharField(max_length=12, null=True, blank=True)
    nome = models.CharField(max_length=100, null=True, blank=True)
    local = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        permissions = (
            ('pode_cadastrar_evento', 'Pode cadastrar evento'),
        )

    def __str__(self):
        return self.nome