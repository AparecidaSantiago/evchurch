from django import forms
from .models import *
from ..instituicoes.models import Instituicao

ESCOLHA_CHOICES = [
    ('-----', '-----'),
    ('Sim', 'Sim'),
    ('Não', 'Não')
]

class SearchEvento(forms.Form):
    """Formulário para consulta de eventos da view que lista eventos
    de forma genérica"""
    nome = forms.CharField(required=False, label="Nome do Evento")
    instituicao = forms.ModelChoiceField(queryset=Instituicao.objects.all(), required=False, label="Instituição")
    local = forms.CharField(required=False, label="Local")
    data_inicial = forms.DateField(required=False)

class EventoCreateForm(forms.ModelForm):
    """Formulário para criação de eventos"""
    responsavel = forms.CharField(required=True)
    instituicao = forms.ModelChoiceField(queryset=Instituicao.objects.all(), required=True, label="Instituição")
    data_inicial = forms.CharField(widget=forms.TextInput(attrs={'type': 'date'}), required=False, label="Data Inicial")
    data_final = forms.CharField(widget=forms.TextInput(attrs={'type': 'date'}), required=False, label="Data Final")
    horario_entrada = forms.CharField(required=True)
    horario_saida = forms.CharField(required=True)
    aberto_publico = forms.ChoiceField(choices=ESCOLHA_CHOICES, required=True, label="Aberto ao Público")

    class Meta:
        model = Evento
        fields = ('__all__')