from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from django.http import HttpResponse
from .forms import *
from .models import Evento

@method_decorator(login_required, name='dispatch')
class EventoView(SearchCustomView):
    """
    View para consulta de Eventos
    """
    form_class = SearchEvento
    model = Evento
    order_field = "nome"
    template_name = "evento/index.html"
    page_title = "Evento"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("evento:evento_index"), "Evento")
    ]

    def get_queryset(self):
        """
        Altera o filtro da evento ::
        """
        queryset = super(EventoView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class EventoDetailView(CustomDetailView):
    """
    View para exibição dos detalhes dos eventos
    """
    model = Evento
    template_name = "evento/detail.html"
    page_title = "Evento"

    def get_context_data(self, **kwargs):
        context = super(EventoDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("evento:evento_index"), "Evento"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
@permission_required("evento.add_evento", raise_exception=True)
def evento_create(request):
    """View para Criar um evento"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("evento:evento_index"), "Evento"),
        (reverse("evento:evento_create"), "Novo Evento")
    ]

    form = EventoCreateForm()

    if request.method == 'POST':
        form = EventoCreateForm(request.POST)
        if form.is_valid():
            evento = form.save(commit=False)
            evento.save()

            messages.success(request, "Evento cadastrado com sucesso")
            return redirect(reverse("evento:evento_show", args=[evento.pk]))

    return render(request, "evento/cadastro.html", locals())

@login_required
@permission_required("evento.add_evento", raise_exception=True)
def evento_edit(request, pk):
    """"View para Editar os eventos"""
    evento = get_object_or_404(Evento, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("evento:evento_index"), "Agenda"),
        (reverse("evento:evento_show", args=[pk]), evento.id),
        ("#", "Editar")
    ]

    form = EventoCreateForm(instance=evento)

    if request.method == 'POST':
        form = EventoCreateForm(request.POST, request.FILES, instance=evento)

        if form.is_valid():
            evento = form.save(commit=False)
            evento.save()

            messages.success(request, "Evento alterado com sucesso.")
            return redirect(reverse("evento:evento_show", args=[evento.pk]))

    return render(request, "evento/editar.html", locals())

@login_required
@permission_required("evento.add_evento", raise_exception=True)
def evento_delete(request, pk):
    """
    View responsável por deletar um evento
    """
    evento = get_object_or_404(Evento, pk=pk)

    evento.delete()
    messages.success(request, "Evento deletado com sucesso!")
    return HttpResponse("Evento deletado com sucesso!")