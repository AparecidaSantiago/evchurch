from django.apps import AppConfig
from django.urls import reverse_lazy
from ..core.menu import Menu

class EventoConfig(AppConfig):
    name = 'evChurch.apps.evento'

    menu = {
        'verbose_name': 'Evento',
        'permissions': ['evento.add_evento'],
        'login_required': 'True',
        'icon': 'alarm-check',
        'items': [
            {
                'verbose_name': 'Pesquisar Evento',
                'link': reverse_lazy('evento:evento_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Evento',
                'link': reverse_lazy('evento:evento_create'),
                'permissions': ['pode_cadastrar_evento'],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)