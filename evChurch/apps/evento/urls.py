from django.urls import path, include
from . import views

app_name='evento'

urlpatterns = [
    path("evento/", views.EventoView.as_view(), name="evento_index"),
    path("evento/detalhes/<int:pk>/",
        views.EventoDetailView.as_view(), name="evento_show"),
    path("evento/cadastrar/",views.evento_create, name="evento_create"),
    path("evento/<int:pk>/editar/",views.evento_edit, name="evento_edit"),
    path("evento/<int:pk>/excluir/", views.evento_delete, name="evento_delete")
]
