from django.urls import path, include

app_name = 'auth'
urlpatterns = [
    path("authorization", include('django.contrib.auth.urls'))
]
