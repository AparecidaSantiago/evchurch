from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page
from django.contrib.auth.decorators import login_required

@login_required
def logout(request):
    logout(request)
    return redirect('registration/login.html')

def login(request):
    login(request)
    return redirect('registration/login.html')