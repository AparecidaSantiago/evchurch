from django.apps import AppConfig


class AuthorizationConfig(AppConfig):
    name = 'evChurch.apps.authorization'
