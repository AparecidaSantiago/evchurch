from django import forms
from .models import *

class SearchAmbientes(forms.Form):
    """Formulário para consulta de ambientes da view que lista ambientes
    de forma genérica"""
    tipo = forms.ModelChoiceField(queryset=TipoAmbiente.objects.all(),required=False, label="Tipo do Ambiente")
    capacidade = forms.IntegerField(required=False)
    descricao = forms.CharField(required=False)

class AmbientesCreateForm(forms.ModelForm):
    """Formulário para criação de ambientes"""
    capacidade = forms.IntegerField(required=True, label="Capacidade")
    tipo = forms.ModelChoiceField(queryset=TipoAmbiente.objects.all(), required=True, label="Tipo de Ambiente")
    descricao = forms.CharField(required=True, label="Descrição")

    class Meta:
        model = Ambientes
        fields = ('__all__')