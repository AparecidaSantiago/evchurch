from django.db import models

class TipoAmbiente(models.Model):
    """Modelo que reune informações sobre os tipos de ambientes"""
    descricao = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.descricao

class Ambientes(models.Model):
    """Modelo que reune informações sobre os ambientes"""
    tipo = models.ForeignKey(TipoAmbiente, null=True, blank=True, on_delete=models.CASCADE)
    capacidade = models.IntegerField(null=True, blank=True)
    descricao = models.TextField(max_length=500, null=True, blank=True)

    class Meta:
        permissions = (
            ('pode_cadastrar_ambientes', 'Pode cadastrar ambientes'),
        )

    def __str__(self):
        return self.descricao