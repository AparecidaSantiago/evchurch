from django.apps import AppConfig
from django.urls import reverse_lazy
from ..core.menu import Menu

class AmbientesConfig(AppConfig):
    name = 'evChurch.apps.ambientes'

    menu = {
        'verbose_name': 'Ambientes',
        'permissions': ['ambientes.add_ambiente'],
        'login_required': 'True',
        'icon': 'home-outline',
        'items': [
            {
                'verbose_name': 'Pesquisar Ambientes',
                'link': reverse_lazy('ambientes:ambientes_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Ambiente',
                'link': reverse_lazy('ambientes:ambientes_create'),
                'permissions': ['pode_cadastrar_ambientes'],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)
