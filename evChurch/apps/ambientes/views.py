from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from .forms import *
from django.http import HttpResponse

@method_decorator(login_required, name='dispatch')
class AmbientesView(SearchCustomView):
    """
    View para consulta de ambientes
    """
    form_class = SearchAmbientes
    model = Ambientes
    order_field = "descricao"
    template_name = "ambientes/index.html"
    page_title = "Ambientes"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("ambientes:ambientes_index"), "Ambientes")
    ]

    def get_queryset(self):
        """
        Altera o filtro do ambiente ::
        """
        queryset = super(AmbientesView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class AmbientesDetailView(CustomDetailView):
    """
    View para exibição dos detalhes dos ambientes
    """
    model = Ambientes
    template_name = "ambientes/detail.html"
    page_title = "Ambientes"

    def get_context_data(self, **kwargs):
        context = super(AmbientesDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("ambientes:ambientes_index"), "Ambiente"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
@permission_required("ambientes.add_ambientes", raise_exception=True)
def ambientes_create(request):
    """View para Criar um ambientes"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("ambientes:ambientes_index"), "Ambientes"),
        (reverse("ambientes:ambientes_create"), "Novo Ambiente")
    ]

    form = AmbientesCreateForm()
    if request.method == 'POST':
        form = AmbientesCreateForm(request.POST)

        if form.is_valid():
            ambiente = form.save(commit=False)
            ambiente.save()

            messages.success(request, "Ambiente cadastrado com sucesso")
            return redirect(reverse("ambientes:ambientes_show", args=[ambiente.pk]))

    return render(request, "ambientes/cadastro.html", locals())

@login_required
@permission_required("ambientes.add_ambientes", raise_exception=True)
def ambientes_edit(request, pk):
    """"View para Editar os ambientes"""
    ambiente = get_object_or_404(Ambientes, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("ambientes:ambientes_index"), "Ambientes"),
        (reverse("ambientes:ambientes_show", args=[pk]), ambiente.id),
        ("#", "Editar")
    ]

    form = AmbientesCreateForm(instance=ambiente)

    if request.method == 'POST':
        form = AmbientesCreateForm(request.POST, request.FILES, instance=ambiente)

        if form.is_valid():
            ambiente = form.save(commit=False)
            ambiente.save()

            messages.success(request, "Ambiente alterado com sucesso.")
            return redirect(reverse("ambientes:ambientes_show", args=[ambiente.pk]))

    return render(request, "ambientes/editar.html", locals())

@login_required
@permission_required("ambientes.add_ambientes", raise_exception=True)
def ambientes_delete(request, pk):
    """
    View responsável por deletar um ambiente
    """
    ambiente = get_object_or_404(Ambientes, pk=pk)

    ambiente.delete()
    messages.success(request, "Ambiente deletado com sucesso!")
    return HttpResponse("Ambiente deletado com sucesso!")