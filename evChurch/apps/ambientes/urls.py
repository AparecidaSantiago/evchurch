from django.urls import path, include
from . import views

app_name='ambientes'

urlpatterns = [
    path("ambientes/", views.AmbientesView.as_view(), name="ambientes_index"),
    path("ambientes/detalhes/<int:pk>/",
        views.AmbientesDetailView.as_view(), name="ambientes_show"),
    path("ambientes/cadastrar/",views.ambientes_create, name="ambientes_create"),
    path("ambientes/<int:pk>/editar/",views.ambientes_edit, name="ambientes_edit"),
    path("ambientes/<int:pk>/excluir/", views.ambientes_delete, name="ambientes_delete")
]