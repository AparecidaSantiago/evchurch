from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from .forms import *
from django.http import HttpResponse

@method_decorator(login_required, name='dispatch')
class GruposView(SearchCustomView):
    """
    View para consulta de grupos
    """
    form_class = SearchGrupos
    model = Grupos
    order_field = "nome"
    template_name = "grupos/index.html"
    page_title = "Grupos"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("grupos:grupos_index"), "Grupos")
    ]

    def get_queryset(self):
        """
        Altera o filtro do grupo ::
        """
        queryset = super(GruposView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class GruposDetailView(CustomDetailView):
    """
    View para exibição dos detalhes dos grupos
    """
    model = Grupos
    template_name = "grupos/detail.html"
    page_title = "Grupos"

    def get_context_data(self, **kwargs):
        context = super(GruposDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("grupos:grupos_index"), "Grupos"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
@permission_required("grupos.add_grupo", raise_exception=True)
def grupos_create(request):
    """View para Criar um grupos"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("grupos:grupos_index"), "Grupos"),
        (reverse("grupos:grupos_create"), "Novo Grupo")
    ]

    form = GruposCreateForm()
    if request.method == 'POST':
        form = GruposCreateForm(request.POST)

        if form.is_valid():
            grupo = form.save(commit=False)
            grupo.save()

            messages.success(request, "Grupo cadastrado com sucesso")
            return redirect(reverse("grupos:grupos_show", args=[grupo.pk]))

    return render(request, "grupos/cadastro.html", locals())

@login_required
@permission_required("grupos.add_grupo", raise_exception=True)
def grupos_edit(request, pk):
    """"View para Editar os Grupos"""
    grupo = get_object_or_404(Grupos, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("grupos:grupos_index"), "Grupos"),
        (reverse("grupos:grupos_show", args=[pk]), grupo.id),
        ("#", "Editar")
    ]

    form = GruposCreateForm(instance=grupo)

    if request.method == 'POST':
        form = GruposCreateForm(request.POST, request.FILES, instance=grupo)

        if form.is_valid():
            grupo = form.save(commit=False)
            grupo.save()

            messages.success(request, "Grupos alterado com sucesso.")
            return redirect(reverse("grupos:grupos_show", args=[grupo.pk]))

    return render(request, "grupos/editar.html", locals())

@login_required
@permission_required("grupos.add_grupo", raise_exception=True)
def grupos_delete(request, pk):
    """
    View responsável por deletar um grupos
    """
    grupo = get_object_or_404(Grupos, pk=pk)

    grupo.delete()
    messages.success(request, "Grupo deletado com sucesso!")
    return HttpResponse("Grupo deletado com sucesso!")
