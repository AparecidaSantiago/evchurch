from django.urls import path, include
from . import views

app_name='grupos'

urlpatterns = [
    path("grupos/", views.GruposView.as_view(), name="grupos_index"),
    path("grupos/detalhes/<int:pk>/",
        views.GruposDetailView.as_view(), name="grupos_show"),
    path("grupos/cadastrar/",views.grupos_create, name="grupos_create"),
    path("grupos/<int:pk>/editar/",views.grupos_edit, name="grupos_edit"),
    path("grupos/<int:pk>/excluir/", views.grupos_delete, name="grupos_delete")
]
