from django import forms
from localflavor.br.forms import BRStateChoiceField
from ..usuarios.models import Usuario
from .models import *

TURNO_CHOICES = [
    ('-----', '-----'),
    ('Matutino', 'Matutino'),
    ('Vespertino', 'Vespertino'),
    ('Noturno', 'Noturno'),
]

DIA_SEMANA_CHOICES = [
    ('-----', '-----'),
    ('Domingo', 'Domingo'),
    ('Segunda-Feira', 'Segunda-Feira'),
    ('Terça-Feira', 'Terça-Feira'),
    ('Quarta-Feira', 'Quarta-Feira'),
    ('Quinta-Feira', 'Quinta-Feira'),
    ('Sexta-Feira', 'Sexta-Feira'),
    ('Sábado', 'Sábado'),
]

class SearchGrupos(forms.Form):
    """Formulário para consulta de grupos da view que lista grupos
    de forma genérica"""

    nome = forms.CharField(max_length=90, required=False)
    lider = forms.CharField(required=False)
    dia_semana = forms.CharField(required=False)
    perfil = forms.ModelChoiceField(queryset=Perfil.objects.all(), required=False, label="Perfil")

class GruposCreateForm(forms.ModelForm):
    """Formulário para criação de grupos"""
    nome = forms.CharField(required=True, label="Nome")
    tipo = forms.ModelChoiceField(queryset=Tipo.objects.all(), required=True, label="Tipo")
    perfil = forms.ModelChoiceField(queryset=Perfil.objects.all(), required=True, label="Perfil")
    lider = forms.CharField(required=True, label="Líder")
    data_abertura = forms.CharField(widget=forms.TextInput(attrs={'type':'date'}), required=False, label="Data de Abertura")
    turno = forms.ChoiceField(choices=TURNO_CHOICES, required=True)
    dia_semana = forms.ChoiceField(choices=DIA_SEMANA_CHOICES, required=True)
    instituicao = forms.ModelChoiceField(queryset=Instituicao.objects.all(), required=True, label="Instituição")
    cep = forms.CharField(required=True)
    logradouro = forms.CharField(required=True)
    numero = forms.CharField(required=True)
    cidade = forms.CharField(required=True)
    bairro = forms.CharField(required=True)
    uf = forms.CharField(required=True, label="UF")

    class Meta:
        model = Grupos
        fields = ('__all__')