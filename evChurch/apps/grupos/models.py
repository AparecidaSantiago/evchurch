from django.db import models
from  ..instituicoes.models import Instituicao

class Tipo(models.Model):
    """Modelo responsável por reunir informações sobre os tipos de grupos"""
    descricao = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.descricao

class Perfil(models.Model):
    """Modelo responsável por reunir informações sobre os perfis dos grupos"""
    descricao = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.descricao

class Grupos(models.Model):
    """Modelo responsavel por reunir informações sobre os grupos de uma instituição"""
    nome = models.CharField(max_length=100, null=True, blank=True)
    tipo = models.ForeignKey(Tipo, null=True, blank=True, on_delete=models.CASCADE)
    data_abertura = models.DateField(auto_now_add=True, null=True, blank=True)
    perfil = models.ForeignKey(Perfil, null=True, blank=True, on_delete=models.CASCADE)
    lider = models.CharField(max_length=100, null=True, blank=True)
    logradouro = models.CharField(max_length=120, null=True, blank=True)
    numero = models.CharField(max_length=10, null=True, blank=True)
    bairro = models.CharField(max_length=100, null=True, blank=True)
    complemento = models.CharField(max_length=80, blank=True, null=True)
    cep = models.CharField(max_length=9, null=True, blank=True)
    cidade = models.CharField(max_length=100, null=True, blank=True)
    telefone = models.CharField(max_length=12, null=True, blank=True)
    uf = models.CharField(max_length=2, null=True, blank=True)
    dia_semana = models.CharField(max_length=50, null=True, blank=True)
    turno =  models.CharField(max_length=20, null=True, blank=True)
    instituicao = models.ForeignKey(Instituicao, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.nome