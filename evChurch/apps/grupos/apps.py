from django.apps import AppConfig
from django.urls import reverse_lazy
from ..core.menu import Menu

class GruposConfig(AppConfig):
    name = 'evChurch.apps.grupos'

    menu = {
        'verbose_name': 'Grupos',
        'permissions': ['grupos.add_grupo'],
        'login_required': 'True',
        'icon': 'account-multiple',
        'items': [
            {
                'verbose_name': 'Pesquisar Grupo',
                'link': reverse_lazy('grupos:grupos_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Grupo',
                'link': reverse_lazy('grupos:grupos_create'),
                'permissions': ['pode_cadastrar_grupo'],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)
