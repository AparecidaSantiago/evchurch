from django.apps import AppConfig
from django.urls import reverse_lazy
from ..core.menu import Menu

class AgendaConfig(AppConfig):
    name = 'evChurch.apps.agenda'

    menu = {
        'verbose_name': 'Agenda',
        'permissions': ['agenda.add_agenda'],
        'login_required': 'True',
        'icon': 'calendar-today',
        'items': [
            {
                'verbose_name': 'Pesquisar Agenda',
                'link': reverse_lazy('agenda:agenda_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Agenda',
                'link': reverse_lazy('agenda:agenda_create'),
                'permissions': ['pode_cadastrar_agenda'],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)