from django import forms
from localflavor.br.forms import BRStateChoiceField
from ..instituicoes.models import Instituicao
from .models import *

ESCOLHA_CHOICES = [
    ('-----', '-----'),
    ('Sim', 'Sim'),
    ('Não', 'Não')
]

class SearchAgenda(forms.Form):
    """Formulário para consulta de agendas da view que lista agendas
    de forma genérica"""

    tipo_reuniao = forms.ModelChoiceField(queryset=TipoReuniao.objects.all(), required=False, label="Reunião")
    responsavel = forms.CharField(required=False)
    instituicao = forms.ModelChoiceField(queryset=Instituicao.objects.all(), required=False, label="Instituição")

class AgendaCreateForm(forms.ModelForm):
    """Formulário para criação de uma agenda"""
    reuniao = forms.ModelChoiceField(queryset=TipoReuniao.objects.all(), required=True, label="Reunião")
    responsavel = forms.CharField(required=True)
    instituicao = forms.ModelChoiceField(queryset=Instituicao.objects.all(), required=True, label="Instituição")
    data_inicial = forms.CharField(widget=forms.TextInput(attrs={'type':'date'}), required=False, label="Data Inicial")
    data_final = forms.CharField(widget=forms.TextInput(attrs={'type':'date'}), required=False, label="Data Final")
    horario_entrada = forms.CharField(required=True)
    horario_saida = forms.CharField(required=True)
    aberto_publico = forms.ChoiceField(choices=ESCOLHA_CHOICES, required=True, label="Aberto ao Público")

    class Meta:
        model = Agenda
        fields = ('__all__')
