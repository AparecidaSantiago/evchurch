# -*- coding: utf-8 -*-
from django.urls import reverse_lazy as reverse
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from ..core.generic_views import SearchCustomView, CustomDetailView
from django.http import HttpResponse
from .forms import *
from .models import Agenda

@method_decorator(login_required, name='dispatch')
class AgendaView(SearchCustomView):
    """
    View para consulta de agendas
    """
    form_class = SearchAgenda
    model = Agenda
    order_field = "titulo"
    template_name = "agenda/index.html"
    page_title = "Agenda"
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("agenda:agenda_index"), "Agenda")
    ]

    def get_queryset(self):
        """
        Altera o filtro da agenda ::
        """
        queryset = super(AgendaView, self).get_queryset()
        form = self.form_class(self.request.GET)
        form.is_valid()
        query = {}

        # Criando nova biblioteca com os valores preenchidos
        for key, value in form.cleaned_data.items():
            if value and value != "":
                query[key] = value

        queryset = queryset.filter(**query)
        return queryset

@method_decorator(login_required, name='dispatch')
class AgendaDetailView(CustomDetailView):
    """
    View para exibição dos detalhes das agendas
    """
    model = Agenda
    template_name = "agenda/detail.html"
    page_title = "Agenda"

    def get_context_data(self, **kwargs):
        context = super(AgendaDetailView, self).get_context_data(**kwargs)
        context["breadcrumbs"] = [
            (reverse("root_path"), "Início"),
            (reverse("agenda:agenda_index"), "Agenda"),
            ("#", self.get_object().id)
        ]

        return context

@login_required
@permission_required("agenda.add_agenda", raise_exception=True)
def agenda_create(request):
    """View para Criar uma agenda"""
    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("agenda:agenda_index"), "Agenda"),
        (reverse("agenda:agenda_create"), "Nova Agenda")
    ]

    form = AgendaCreateForm()

    if request.method == 'POST':
        form = AgendaCreateForm(request.POST)
        if form.is_valid():
            agenda = form.save(commit=False)
            agenda.save()

            messages.success(request, "Agenda cadastrada com sucesso")
            return redirect(reverse("agenda:agenda_show", args=[agenda.pk]))

    return render(request, "agenda/cadastro.html", locals())

@login_required
@permission_required("agenda.add_agenda", raise_exception=True)
def agenda_edit(request, pk):
    """"View para Editar as Agendas"""
    agenda = get_object_or_404(Agenda, pk=pk)

    breadcrumbs = [
        (reverse("root_path"), "Início"),
        (reverse("agenda:agenda_index"), "Agenda"),
        (reverse("agenda:agenda_show", args=[pk]), agenda.id),
        ("#", "Editar")
    ]

    form = AgendaCreateForm(instance=agenda)

    if request.method == 'POST':
        form = AgendaCreateForm(request.POST, request.FILES, instance=agenda)

        if form.is_valid():
            agenda = form.save(commit=False)
            agenda.save()

            messages.success(request, "Agenda alterado com sucesso.")
            return redirect(reverse("agenda:agenda_show", args=[agenda.pk]))

    return render(request, "agenda/editar.html", locals())

@login_required
@permission_required("agenda.add_agenda", raise_exception=True)
def agenda_delete(request, pk):
    """
    View responsável por deletar uma agenda
    """
    agenda = get_object_or_404(Agenda, pk=pk)

    agenda.delete()
    messages.success(request, "Agenda deletada com sucesso!")
    return HttpResponse("Agenda deletada com sucesso!")
