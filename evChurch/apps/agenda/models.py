from django.db import models
from ..instituicoes.models import Instituicao
from ..usuarios.models import Usuario

class TipoReuniao(models.Model):
    """Modelo responsável por reunir informações sobre os tipos de reuniões da instituição"""
    descricao = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.descricao

class Agenda(models.Model):
    """Modelo responsável por reunir informações sobre a agenda da instituição"""
    instituicao = models.ForeignKey(Instituicao, null=True, blank=True, on_delete=models.CASCADE)
    reuniao = models.ForeignKey(TipoReuniao, null=True, blank=True, on_delete=models.CASCADE)
    data_inicial = models.DateField()
    data_final = models.DateField()
    horario_entrada = models.CharField(max_length=5, null=True, blank=True)
    horario_saida = models.CharField(max_length=5, null=True, blank=True)
    responsavel = models.CharField(max_length=100, null=True, blank=True)
    aberto_publico = models.CharField(max_length=3, null=True, blank=True)
    telefone = models.CharField(max_length=12, null=True, blank=True)
    titulo = models.CharField(max_length=100, null=True, blank=True)
    descricao = models.CharField(max_length=500, null=True, blank=True)
    local = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.titulo

class DiaSemana(models.Model):
    """Modelo responsavel por agrupar os dias da semana"""
    descricao = models.CharField(max_length=200, null=True, blank=True)
    agenda = models.ForeignKey(Agenda, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.descricao