from django.urls import path, include
from . import views

app_name='agenda'

urlpatterns = [
    path("agenda/", views.AgendaView.as_view(), name="agenda_index"),
    path("agenda/detalhes/<int:pk>/",
        views.AgendaDetailView.as_view(), name="agenda_show"),
    path("agenda/cadastrar/",views.agenda_create, name="agenda_create"),
    path("agenda/<int:pk>/editar/",views.agenda_edit, name="agenda_edit"),
    path("agenda/<int:pk>/excluir/", views.agenda_delete, name="agenda_delete")
]
