# -*- coding: utf-8 -*-
import re
from django import template
from django.urls import reverse_lazy
from ..menu import Menu


register = template.Library()

@register.simple_tag(takes_context=True)
def get_menu(context):
    menu = Menu.get_instance()
    context['app_menu'] = menu.get_app_menu()
    return ''

@register.simple_tag(takes_context=True)
def has_permission(context, permissions):
    user = context.get('user')
    return user.has_perms(permissions)

@register.filter(name="normalize_paginator_debug")
def normalize_paginator_debug(paginator, current_page):
    import pdb; pdb.set_trace()
    return paginator

@register.filter(name="normalize_paginator")
def normalize_paginator(paginator, page_object=None):
    """
    Filtro para normalização de apresentação referente ao número de páginas que
    serão exibidas em no documento HTML. Este filtro é necessário quando o número
    de páginas de uma páginação é demasiadamente grande.

    paginator: Paginator (Django builtin paginator)
    page_object: Page (Paginator Current page)
    """
    pages = list(paginator.page_range)

    if page_object.number <= 5:
        return pages[0:page_object.number] + pages[page_object.number:page_object.number+5]
    else:
        return pages[page_object.number-5:page_object.number] + pages[page_object.number:page_object.number+5]

@register.filter(name="get_breadcrumb")
def get_breadcrumb(object_item=None, object_list=None):

    paths = [
        (reverse_lazy("root_path"), "Home"),
    ]

    if object_item is None and object_list is None:
        return paths

    # if isinstance(list)
    return paths

@register.filter(name="get_url_params")
def get_url_params(params):
    return re.sub(r"page=\d+&", "", params)

@register.simple_tag()
def menu_is_used(items, path):
    """Verifica se o link do request está contido em algum item do menu"""
    for item in items:
        if str(item.get("link")) in path:
            return True
    return False

@register.filter
def in_list(value, the_list):
    value = str(value)
    return value in the_list
