from django.test import TestCase, Client
from django.contrib.auth.models import User
from model_mommy import mommy
from .menu import Menu
from .templatetags import core as templatetag
from .utils import get_user_organizational_unit

class CoreTest(TestCase):
    """Testes do módulo core"""

    def setUp(self):
        self.basic_user = {'username': 'teste', 'password': 'acesso@teste'}
        self.client = Client()
        self.sidebar = Menu.get_instance()
        self.menu = {
            'verbose_name': 'First Menu',
            'permissions': [],
            'items': [

            ]
        }

    def test_menu_singleton_object(self):
        second_sidebar = Menu.get_instance()
        self.assertEquals(self.sidebar, second_sidebar)

    def test_menu_rendering(self):
        sidebar = Menu()
        sidebar.add_app_menu(self.menu)
        self.assertEquals(sidebar.get_app_menu(), [self.menu])

    def test_menu_permissions(self):
        user = mommy.make(User)
        context = {'user': user}
        self.assertTrue(templatetag.has_permission(context, self.menu.get('permissions')))


class UtilityTests(TestCase):
    """Testes das funções utilitárias do módulo Core"""

    def test_should_get_user_organizational_unit(self):
        """Testa a busca da unidade organizacional para usuários do LDAP"""

        unit = get_user_organizational_unit("teste")
        self.assertEqual("DITI", unit.get("organizational_unit"))

        with self.assertRaises(Exception) as context:
            unit = get_user_organizational_unit("not_found")

            self.assertTrue("User not found" in context.exception)

        with self.assertRaises(Exception) as context:
            unit = get_user_organizational_unit()

            self.assertTrue("The user can\'t be blank" in context.exception)
