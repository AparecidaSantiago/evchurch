# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.decorators.cache import cache_page
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

@login_required
def index(request):
    return render(request, 'index.html', locals())

@login_required
def home(request):
    """View padrão do projeto"""
    # Armazena data do último login do usuário logado
    last_login = request.user.last_login
    # Armazena nome completo do usuário logado
    usuario = request.user.first_name

    return render(request, "home.html", locals())
