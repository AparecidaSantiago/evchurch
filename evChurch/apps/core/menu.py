class Menu():
    """
    Responsible to generate and storage our front-end menu application
    usage:
    menu = Menu.get_instance()

    """
    _instance = None
    _menu = None

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._menu = []

    @staticmethod
    def get_instance():
        if Menu._instance is None:
            Menu._instance = Menu()

        return Menu._instance

    def add_app_menu(self, obj):
        """
        Add a menu object to our list
        it should have this struct
        {'verbose_name': String, 'permissions': list, 'items': list}
        """
        for item in obj.get('items'):
            if item.get('private', None) is None:
                item.update({'private': True})

        self._menu.append(obj)

    def get_app_menu(self):
        return self._menu
