from django.apps import AppConfig

class CoreConfig(AppConfig):
    name = 'evChurch.apps.core'
