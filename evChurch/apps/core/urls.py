# -*- coding: utf-8 -*-
from . import views
from django.urls import path

app_name = 'core'
urlpatterns = [
    path("core", views.index, name='index'),
]
