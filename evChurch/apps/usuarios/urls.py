from django.urls import path, include
from . import views

app_name='usuarios'

urlpatterns = [
    path("usuario/", views.UsuarioView.as_view(), name="usuario_index"),
    path("usuario/detalhes/<int:pk>/",
        views.UsuarioDetailView.as_view(), name="usuario_show"),
    path("usuario/cadastrar/",views.usuario_create, name="usuario_create"),
    path("usuario/<int:pk>/editar/",views.usuario_edit, name="usuario_edit"),
    path("usuario/<int:pk>/excluir/", views.usuario_delete, name="usuario_delete")
]
