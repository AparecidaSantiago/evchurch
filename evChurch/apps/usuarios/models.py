from django.db import models
from ..instituicoes.models import Instituicao

class Perfil(models.Model):
    descricao = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.descricao

class Usuario(models.Model):
    nome = models.CharField(max_length=90, null=True, blank=True)
    telefone = models.CharField(max_length=20, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    perfil = models.ForeignKey(Perfil, null=True, blank=True, on_delete=models.CASCADE)
    instituicao = models.ForeignKey(Instituicao, null=True, blank=True, on_delete=models.CASCADE)
    logradouro = models.CharField(max_length=120, null=True, blank=True)
    numero = models.CharField(max_length=10, null=True, blank=True)
    bairro = models.CharField(max_length=100, null=True, blank=True)
    complemento = models.CharField(max_length=80, blank=True, null=True)
    cep = models.CharField(max_length=9, null=True, blank=True)
    cidade = models.CharField(max_length=100, null=True, blank=True)
    telefone = models.CharField(max_length=12, null=True, blank=True)
    uf = models.CharField(max_length=2, null=True, blank=True)

    class Meta:
        permissions = (
            ("pode_cadastrar_usuario", "Pode cadastrar usuário"),
            ("pode_alterar_usuario", "Pode editar usuário"),
            ("pode_excluir_usuario", "Pode excluir usuário"),
        )

    def __str__(self):
        return self.nome
