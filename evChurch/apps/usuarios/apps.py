from django.apps import AppConfig
from django.urls import reverse_lazy
from ..core.menu import Menu


class UsuariosConfig(AppConfig):
    name = 'evChurch.apps.usuarios'
    menu = {
        'verbose_name': 'Usuários',
        'permissions': ['usuarios.add_usuario'],
        'login_required': 'True',
        'icon': 'account-card-details',
        'items': [
            {
                'verbose_name': 'Pesquisar Usuário',
                'link': reverse_lazy('usuarios:usuario_index'),
                'permissions': [],
            },
            {
                'verbose_name': 'Cadastrar Usuário',
                'link': reverse_lazy('usuarios:usuario_create'),
                'permissions': ['pode_cadastrar_usuario'],
            }
        ]
    }

    sidebar = Menu.get_instance()
    sidebar.add_app_menu(menu)