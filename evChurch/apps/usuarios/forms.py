# -*- coding: utf-8 -*-
from django import forms
from .models import Usuario
from localflavor.br.forms import BRStateChoiceField
from ..instituicoes.models import Instituicao

class SearchUsuario(forms.Form):
    """Formulário para consulta de usuários da view que lista usuários
    de forma genérica"""

    nome = forms.CharField(max_length=90, required=False)
    email = forms.EmailField(required=False)

class UsuarioCreateForm(forms.ModelForm):
    """Formulário para criação de ocorrências"""
    email = forms.EmailField(required=True, label="Email")
    instituicao = forms.ModelChoiceField(queryset=Instituicao.objects.all(), required=True, label="Instituição")

    class Meta:
        model = Usuario
        fields = ('__all__')

