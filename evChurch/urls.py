"""evChurch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.decorators import login_required
from .apps.core.views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', login_required(home), name="root_path"),
    path('core/', include('evChurch.apps.core.urls', namespace='core')),
    path('auth/',
        include('evChurch.apps.authorization.urls', namespace='authorization')),
    path('usuarios/',
        include('evChurch.apps.usuarios.urls', namespace='usuarios')),
    path('instituicoes/',
        include('evChurch.apps.instituicoes.urls', namespace='instituicoes')),
    path('grupos/',
        include('evChurch.apps.grupos.urls', namespace='grupos')),
    path('agenda/',
         include('evChurch.apps.agenda.urls', namespace='agenda')),
    path('ambientes/',
         include('evChurch.apps.ambientes.urls', namespace='ambientes')),
    path('evento/',
         include('evChurch.apps.evento.urls', namespace='evento'))
]